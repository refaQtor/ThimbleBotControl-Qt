/****************************************************************************
* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this file,
* You can obtain one at http://mozilla.org/MPL/2.0/.
*
* Copyright (c) 2017, shannon.mackey@refaqtory.com
* ***************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUdpSocket>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

private slots:

    void on_Forward_clicked();

    void on_Back_clicked();

    void on_Right_clicked();

    void on_Left_clicked();

    void on_ipaddress_clicked();

private:
    Ui::MainWindow* ui;
    QUdpSocket* udpSocket;
    QHostAddress* host;
    quint16 port;

    QString forward;
    QString back;
    QString right;
    QString left;
};

#endif // MAINWINDOW_H
