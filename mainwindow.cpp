/****************************************************************************
* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this file,
* You can obtain one at http://mozilla.org/MPL/2.0/.
*
* Copyright (c) 2017, shannon.mackey@refaqtory.com
* ***************************************************************************/
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QInputDialog>
#include <QProcess>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , udpSocket(new QUdpSocket(this))
    , host(new QHostAddress())
    , port(3333)
    , forward("w")
    , back("s")
    , right("d")
    , left("a")
{
    ui->setupUi(this);

    connect(udpSocket, &QUdpSocket::hostFound, this, [=]() {
        ui->ipaddress->setChecked(true);
        ui->ipaddress->setStyleSheet("background-color: rgb(10, 100, 4);");
    });
    ui->ipaddress->setChecked(false);
    on_ipaddress_clicked();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_Forward_clicked()
{
    qDebug() << "udp write return: " << udpSocket->write(forward.toLatin1(), forward.length());
}

void MainWindow::on_Back_clicked()
{
    qDebug() << "udp write return: " << udpSocket->write(back.toLatin1(), back.length());
}

void MainWindow::on_Right_clicked()
{
    qDebug() << "udp write return: " << udpSocket->write(right.toLatin1(), right.length());
}

void MainWindow::on_Left_clicked()
{
    qDebug() << "udp write return: " << udpSocket->write(left.toLatin1(), left.length());
}

void MainWindow::on_ipaddress_clicked()
{
    QString newHost = QInputDialog::getText(this, "thimbleBot", "enter IP address", QLineEdit::Normal, "192.168.1.");
    host->setAddress(newHost);
    udpSocket->connectToHost(*host, port);
    ui->ipaddress->setText(newHost);
}
